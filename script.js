const gameContainer = document.getElementById("game");
let cards;
let colors = [];
let shuffledColors;
let color1 = '';
let color2 = '';
let first, second;
let stop = false;
let secondClick = false;
let score = 0;
let cardsMatched = 0;
let checkOver;


function startgame(){
  cards = document.getElementById('cardNumber').value;
  if(cards%2 !==0 || cards === ''){
    document.getElementById('error').innerText = 'Enter a valid number'
    document.getElementById('error').style.color = 'Red';
  }else{
    for(var card = 0; card<cards/2; card++){
      let color = getRandomColor();
      colors.push(color);
      colors.push(color);
    }
    shuffledColors = shuffle(colors);
    createDivsForColors(shuffledColors);
    hideVisibilityOfStartButton();
  }
}


// here is a helper function to shuffle an array
// it returns the same array with values shuffled
// it is based on an algorithm called Fisher Yates if you want ot research more
function shuffle(array) {
  let counter = array.length;

  // While there are elements in the array
  while (counter > 0) {
    // Pick a random index
    let index = Math.floor(Math.random() * counter);

    // Decrease counter by 1
    counter--;

    // And swap the last element with it
    let temp = array[counter];
    array[counter] = array[index];
    array[index] = temp;
  }

  return array;
}

// this function loops over the array of colors
// it creates a new div and gives it a class with the value of the color
// it also adds an event listener for a click for each card
function createDivsForColors(colorArray) {
  for (let color of colorArray) {
    // create a new div
    const newDiv = document.createElement("div");

    // give it a class attribute for the value we are looping over
    newDiv.classList.add(color);
    newDiv.id = Math.random()
    newDiv.style.backgroundColor = '#003459'
    // call a function handleCardClick when a div is clicked on
    newDiv.addEventListener("click", handleCardClick);

    // append the div to the element with an id of game
    gameContainer.append(newDiv);
  }
}

// TODO: Implement this function!

 function handleCardClick(event) {
  if(stop == true){
    return
  }
  document.getElementById('reset').style.visibility = 'visible';
  document.getElementById('exit').style.display = 'none';
// handle first click
  if(secondClick == false){
    color1 = this.className;
    document.getElementById(this.id).style.backgroundColor = this.className;
    secondClick = true;
    first = this;
  }else{
    // handle second click
    stop = true;
    color2 = this.className;
    document.getElementById(this.id).style.backgroundColor = this.className;
    second = this;
    score += 1;
    checkColor()
    secondClick = false;
  }
}

function checkColor(){
  if(first.id!==second.id && color1 === color2){
    first.removeEventListener('click', handleCardClick);
    second.removeEventListener('click', handleCardClick);
    cardsMatched += 2;
    checkIfOver();
    stop = false;
  }
  else{
    setTimeout(()=>{
      document.getElementById(first.id).style.backgroundColor = '#003459';
      document.getElementById(second.id).style.backgroundColor = '#003459';
      stop = false;
    }, 1000);
  }
}



function checkIfOver(){
  if(cardsMatched === +cards){
    checkOver = 1;
    let gameOverMsg =  `Game Over! Your score is ${score}`;
    document.getElementById("scoreBoard").innerHTML = gameOverMsg;
    console.log(cards)
    switch(cards){
      case '6':
        fetch('https://fierce-woodland-20643.herokuapp.com/scores/updateEasy', {
          method: 'POST',
          headers: {
            'Accept':'application/json',
            'Content-type': 'application/json' // The type of data you're sending
          },
          body: JSON.stringify({easy:score}),
          referrerPolicy: 'no-referrer'
        });
        break;
      case '10':
        fetch('https://fierce-woodland-20643.herokuapp.com/scores/updateHard', {
          method: 'POST',
          body: {hard:score},
          headers: {
            'Content-type': 'application/json' // The type of data you're sending
          },
          referrerPolicy: 'no-referrer'
        });
        break;
      case '16':
        fetch('https://fierce-woodland-20643.herokuapp.com/scores/updateExpert', {
          method: 'POST',
          body: score,
          headers: {
            'Content-type': 'application/json' // The type of data you're sending
          }
        });
        break;
    }
  }
}

// reload page
function reloadPage(){
  window.location.reload();
}

function exit(){
  if(checkOver !== 1){
    document.getElementById('exit').style.display = 'flex';
    document.getElementById('reset').style.visibility = 'hidden'; 
  }else{
    reloadPage();
  }
}

function hideExit(){
  document.getElementById('reset').style.visibility = 'visible';
  document.getElementById('exit').style.display = 'none';
}


// create random color
function getRandomColor() {
  var letters = '0123456789ABCDEF';
  var color = '#';
  for (var i = 0; i < 6; i++) {
    color += letters[Math.floor(Math.random() * 16)];
  }
  return color;
}

function hideVisibilityOfStartButton(){
  document.getElementById('cardLabel').style.visibility = 'hidden';
  document.getElementById('cardNumber').style.visibility = 'hidden';
  document.getElementById('startButton').style.visibility = 'hidden';
  document.getElementById('reset').style.visibility = 'visible';
  document.getElementById('cardContainer').style.visibility = 'visible';
  document.getElementById('highScoresButton').style.visibility = 'hidden';
  document.getElementById('brainImg').style.visibility = 'hidden';
  document.getElementById('highScoresDiv').style.visibility = 'hidden';
  
}

function showHighScores(){
  let highScoresDiv = document.getElementById('highScoresDiv');
 

  if (highScoresDiv.style.visibility === "hidden") {
    document.getElementById('highScoresDiv').style.visibility = 'visible';
    document.getElementById('brainImg').style.visibility = 'hidden';
    document.getElementById('highScoresButton').innerText = 'Back';
    fetchScores('/scores/easyHighScores', 'easyScore');
    fetchScores('/scores/hardHighScores', 'hardScore');
    fetchScores('/scores/expertHighScores', 'expertScore');
  } else {
    document.getElementById('highScoresButton').innerText = 'High Scores';
    highScoresDiv.style.visibility = 'hidden';
    document.getElementById('brainImg').style.visibility = 'visible';
  }
}

function fetchScores(path, id){
  fetch('https://fierce-woodland-20643.herokuapp.com'+path)
  .then((data) => {
    return data.json();
  })
  .then((data) => {
    console.log(Object.keys(data))
    document.getElementById(id).innerText = Object.values(data)[0].scores|| 'NA';
  })
  .catch((err)=>{
    document.getElementById(id).innerText = 'NA';
  });

}

function hideHighScores(){
  document.getElementById('highScoresDiv').style.visibility = 'hidden';
  document.getElementById('highScoresButton').style.visibility = 'visible';
  document.getElementById('hideScoresButton').style.visibility = 'hidden';
}